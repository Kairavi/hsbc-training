public class billWithTax {

    public static void main(String args[]) {
    
        String billBeforeTax = args[0];
        String inputstateCode = args[1];
        
        

        double billToBeTax = Float.valueOf(billBeforeTax);
        String stateCode = inputstateCode.toUpperCase();

        if(inputstateCode == "KA"){
            billToBeTax = billToBeTax * 1.15;
        }
        else if(inputstateCode == "TN"){
            billToBeTax = billToBeTax * 1.18;
        }
        else if(inputstateCode == "MH"){
            billToBeTax = billToBeTax * 1.12;
        }
        else{
            billToBeTax = billToBeTax * 1.12;
        }
        System.out.println("The total bill amount is" +" "+"$" +billToBeTax);
    }
}
